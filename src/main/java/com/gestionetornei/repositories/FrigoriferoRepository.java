package com.gestionetornei.repositories;

import com.gestionetornei.model.Frigorifero;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

public interface FrigoriferoRepository extends CrudRepository<Frigorifero, Long>, QueryByExampleExecutor<Frigorifero> {

    public Frigorifero findByIdfrigorifero(Long id);
}
