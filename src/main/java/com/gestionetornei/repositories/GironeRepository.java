package com.gestionetornei.repositories;

import com.gestionetornei.model.Girone;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

public interface GironeRepository extends CrudRepository<Girone, Long>, QueryByExampleExecutor<Girone> {

    Girone findById(long id);
}
