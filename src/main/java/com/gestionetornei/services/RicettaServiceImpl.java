package com.gestionetornei.services;

import com.gestionetornei.model.Ingrediente;
import com.gestionetornei.model.Ricetta;
import com.gestionetornei.repositories.RicettaRespository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
@Service
public class RicettaServiceImpl implements RicettaService{

    @Autowired
    private RicettaRespository ricettaRepository;

    @Override
    @Transactional
    public List<Ricetta> listAllRicette() {

        return (List<Ricetta>) ricettaRepository.findAll();
    }

    @Override
    @Transactional
    public Ricetta caricaSingolaRicetta(Long id) {
        return  ricettaRepository.findByIdricetta(id);

    }

    @Override
    @Transactional
    public void aggiornaRicetta(Ricetta ricettaInstance) {
        ricettaRepository.save(ricettaInstance);
    }

    @Override
    @Transactional
    public void inserisciNuovaRicetta(Ricetta ricettaInstance) {
        ricettaRepository.save(ricettaInstance);

    }

    @Override
    @Transactional
    public void rimuovi(Ricetta ricettaInstance) {
        ricettaRepository.delete(ricettaInstance);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Ricetta> ricercaNome(String input) {
        return ricettaRepository.findRicettaByNomericettaContains(input);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Ricetta> ricercaPerPersone(Integer input) {
        return ricettaRepository.findRicettaByPerpersone(input);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Ricetta> ricercaPerIngredientePrincipale(String input) {
        return ricettaRepository.findRicettaByIngredientePrincipaleContains(input);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Ricetta> ricercaPerIngredientiGenerali(String input) {
        return ricettaRepository.findRicettaByIngredientiGeneraliContains(input);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Ricetta> ricercaPerTipoPiatto(String input) {
        return ricettaRepository.findRicettaByTipologiaPiattoContains(input);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Ricetta> ricercaPerPreparazione(String input) {
        return ricettaRepository.findRicettaByPreparazioneContains(input);
    }


    @Override
    @Transactional(readOnly = true)
    public List<Ricetta> ricercaPerNomeETipo(String nome, String tipo) {
        return ricettaRepository.findRicettaByNomericettaContainsAndTipologiaPiattoContains(nome,tipo);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Ricetta> ricercaPerNomeEIngredientePrincipale(String nome, String ingredientePrincipale) {
        return ricettaRepository.findRicettaByNomericettaContainsAndIngredientePrincipaleContains(nome, ingredientePrincipale);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Ricetta> ricercaPerTipoEIngredientePrincipale(String tipo, String ingredientePrincipale) {
        return ricettaRepository.findRicettaByTipologiaPiattoContainsAndIngredientePrincipaleContains(tipo, ingredientePrincipale);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Ricetta> ricercaPerNomeTipoEIngredientePrincipale(String nome, String ingredientePrincipale, String tipo) {
        return ricettaRepository.findRicettaByNomericettaContainsAndIngredientePrincipaleContainsAndTipologiaPiattoContains(nome, ingredientePrincipale, tipo);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Ricetta> ricercaPerIngredienti(List<Ingrediente> ingredienti) {
        return ricettaRepository.findRicettaByIngredienti(ingredienti);
    }
}
