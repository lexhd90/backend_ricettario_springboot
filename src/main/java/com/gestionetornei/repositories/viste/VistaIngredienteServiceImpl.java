package com.gestionetornei.repositories.viste;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class VistaIngredienteServiceImpl {
    @Autowired
    VistaIngredienteRepository vistaIngredienteRepository;

    @Autowired
    VistaRicercaIngredienteRepository vistaRicercaIngredienteRepository;

    @Transactional(readOnly = true)
    public List<VistaIngrediente>elencaVisteIngredienti() {

        return (List<VistaIngrediente>) vistaIngredienteRepository.findAll();
    }

    @Transactional(readOnly = true)
    public List<VistaRicercaIngrediente>elencaVisteRicercaIngredienti(String nome) {

        return (List<VistaRicercaIngrediente>) vistaRicercaIngredienteRepository.findVistaRicercaIngredienteByNomeContains(nome);
    }

}
