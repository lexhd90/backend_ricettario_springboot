package com.gestionetornei.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Frigorifero {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long idfrigorifero;

    private String nomeFrigorifero;

    @OneToOne(cascade = CascadeType.ALL)
    @JsonIgnoreProperties(value= {"frigorifero"})
    private Utente utente;

    @ManyToMany(mappedBy = "frigoriferi")
    @JsonIgnoreProperties(value= {"frigoriferi"})
    private List<Ingrediente> ingredienti;

    public Frigorifero() {}

    public Long getIdfrigorifero() {
        return idfrigorifero;
    }

    public void setIdfrigorifero(Long idfrigorifero) {
        this.idfrigorifero = idfrigorifero;
    }

    public String getNomeFrigorifero() {
        return nomeFrigorifero;
    }

    public void setNomeFrigorifero(String nomeFrigorifero) {
        this.nomeFrigorifero = nomeFrigorifero;
    }

    public Utente getUtente() {
        return utente;
    }

    public void setUtente(Utente utente) {
        this.utente = utente;
    }

    public List<Ingrediente> getIngredienti() {
        return ingredienti;
    }

    public void setIngredienti(List<Ingrediente> ingredienti) {
        this.ingredienti = ingredienti;
    }
}
