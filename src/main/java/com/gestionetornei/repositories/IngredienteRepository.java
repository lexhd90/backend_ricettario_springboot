package com.gestionetornei.repositories;

import com.gestionetornei.model.Ingrediente;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import java.util.List;

public interface IngredienteRepository extends CrudRepository<Ingrediente, Long>, QueryByExampleExecutor<Ingrediente> {

    public Ingrediente findByIdIngrediente(Long id);
    public List<Ingrediente> findIngredienteByNomeContains(String nome);
}
