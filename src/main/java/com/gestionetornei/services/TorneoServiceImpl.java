package com.gestionetornei.services;

import com.gestionetornei.model.Torneo;
import com.gestionetornei.repositories.TorneoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;
@Service
public class TorneoServiceImpl implements TorneoService {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private TorneoRepository torneoRepository;


    @Override
    @Transactional(readOnly = true)
    public List<Torneo> listAllTornei() {
        return (List<Torneo>) torneoRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Torneo caricaSingoloTorneo(long id) {

//       return torneoRepository.findById(id);
        return torneoRepository.findOneEager(id);
    }

    @Override
    @Transactional
    public void aggiorna(Torneo torneoInstance) {
        torneoRepository.save(torneoInstance);
    }

    @Override
    @Transactional
    public void inserisciNuovoTorneo(Torneo torneoInstance) {
        System.out.println(torneoInstance);
        torneoRepository.save(torneoInstance);
    }

    @Override
    @Transactional
    public void rimuovi(Torneo torneoInstance) {
        torneoRepository.delete(torneoInstance);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Torneo> findByExample(Torneo torneoInstance){
        Query query;
        String domandaAlDb = "select distinct t from Torneo t left join fetch t.squadre s where t.id is not null ";

        if (StringUtils.isNotEmpty(torneoInstance.getNomeTorneo())) {
            domandaAlDb += " AND t.nomeTorneo like '"+"%" + torneoInstance.getNomeTorneo() +"%"+ "'";
        }
        if (StringUtils.isNotEmpty(torneoInstance.getNumeroSquadre()+"".trim())) {
            domandaAlDb += " AND t.numeroSquadre >= '" + torneoInstance.getNumeroSquadre() + "'";
        }


        List<Long> ids = new ArrayList<>();
        if(!torneoInstance.getSquadre().isEmpty()) {
           /* for (Squadra squadraItem : torneoInstance.getSquadre()) {
                ids.add(squadraItem.getId());
            }*/
            domandaAlDb += " and c.id in :idSquadra ";
        }
        query = entityManager.createQuery(domandaAlDb);

        if(!ids.isEmpty())
            query.setParameter("idSquadra", ids);

        return (List<Torneo>) query.getResultList();

    }


}
