package com.gestionetornei.services;

import com.gestionetornei.model.Girone;
import com.gestionetornei.model.Squadra;

import java.util.List;

public interface GironeService {

    public List<Girone> listAllGironi();

    public Girone caricaSingoloGirone(long id);

    public void aggiornaGirone(Girone gironeInstance);

    public void inserisciNuovoGirone(Girone gironeInstance);

    public void rimuovi(Girone gironeInstance);
}
