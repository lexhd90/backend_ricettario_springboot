package com.gestionetornei.services;

import com.gestionetornei.model.Evento;
import com.gestionetornei.model.Girone;

import java.util.List;

public interface EventoService {

    public List<Evento> listAllEventi();

    public Evento caricaSingoloEvento(long id);

    public void aggiornaEvento(Evento eventoInstance);

    public void inserisciNuovoEvento(Evento eventoInstance);

    public void rimuovi(Evento eventoInstance);
}
