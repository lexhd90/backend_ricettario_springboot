package com.gestionetornei.services;


import java.util.List;

import com.gestionetornei.model.Utente;
import com.gestionetornei.repositories.UtenteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;



@Service
public class UtenteServiceImpl implements UtenteService {

    @Autowired
    private UtenteRepository utenteRepository;

    @Transactional(readOnly = true)
    public List<Utente> listAllUtenti() {
        return (List<Utente>) utenteRepository.findAll();
    }

    @Transactional(readOnly = true)
    public Utente caricaSingoloUtente(Long id) {
        return utenteRepository.findUtenteById(id);
    }

    @Transactional
    public void aggiorna(Utente utenteInstance) {
        utenteRepository.save(utenteInstance);
    }

    @Transactional
    public void inserisciNuovo(Utente utenteInstance) {
        utenteRepository.save(utenteInstance);
    }

    @Transactional
    public void rimuovi(Utente utenteInstance) {
        utenteRepository.delete(utenteInstance);

    }

    @Transactional(readOnly = true)
    public List<Utente> findByExample(Utente example) {
        // TODO Auto-generated method stub
        return null;
    }

    @Transactional(readOnly = true)
    public Utente eseguiAccesso(String username, String password) {
        return utenteRepository.findByUsernameAndPassword(username, password);
    }

//    @Override
//    public void rimuoviId(Long id) {
//        utenteRepository.delete(id);
//
//    }



}
