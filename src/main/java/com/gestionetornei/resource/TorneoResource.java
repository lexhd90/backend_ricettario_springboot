package com.gestionetornei.resource;

import com.gestionetornei.model.Torneo;
import com.gestionetornei.services.TorneoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;


@RestController
@RequestMapping("tornei")
public class TorneoResource {

    @Autowired
    TorneoService torneoService;


    @GetMapping(value = "/{id}", produces=MediaType.APPLICATION_JSON)
    public Torneo getTorneo(@PathVariable Long id){
        System.out.println(id);
        return torneoService.caricaSingoloTorneo(id);

    }



    @GetMapping(path = "/lista", produces=MediaType.APPLICATION_JSON)
    public List<Torneo> listAll() {
        System.out.println("prova");
        return torneoService.listAllTornei();
    }


    @PostMapping("/inserisci")
    @Produces(MediaType.APPLICATION_JSON)
    public boolean inserisciTorneo(@RequestBody Torneo torneoInput) {
        System.out.println(torneoInput);
//        if ((torneoInput.validate()) && (torneoInput.getSquadre().size()== torneoInput.getNumeroSquadre())) {

            System.out.println("sono nell'inserimento");
            torneoService.inserisciNuovoTorneo(torneoInput);



        return false;

    }

    @PutMapping(value = "/aggiorna")
    @Produces(MediaType.APPLICATION_JSON)
    public boolean updateTorneo(@RequestBody Torneo torneoInput) {

        System.out.println("SONO NEL METODO MODIFICA DEL TORNEO!!!! " +torneoInput);
        if (torneoInput.validate()) {
            System.out.println("sono qui nell'if");
            torneoService.aggiorna(torneoInput);
            return true;
        }
        System.out.println("sono fuori dall'if");
        return false;

    }

    @PutMapping(value = "/aggiorna2")
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateTorneo2(@RequestBody Torneo torneoInput) {

        System.out.println("..........................SONO NEL METODO MODIFICA DEL TORNEO!!!! " +torneoInput);
        if (torneoInput.validate()) {
            System.out.println("sono qui nell'if");
            torneoService.aggiorna(torneoInput);
            return Response.ok().build();
        }
        System.out.println("sono fuori dall'if");

        return Response.serverError().entity("Errore!!").build();
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON)
    public boolean delete(){
        return false;

    }

}
