package com.gestionetornei.services;

import com.gestionetornei.model.Ingrediente;
import com.gestionetornei.repositories.IngredienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class IngredienteServiceImpl implements  IngredienteService{

    @Autowired
    private IngredienteRepository ingredienteRepository;

    @Override
    @Transactional
    public List<Ingrediente> listAllIngredienti() {
        return (List<Ingrediente>) ingredienteRepository.findAll();
    }

    @Override
    @Transactional
    public Ingrediente caricaSingolaIngrediente(Long id) {
        return ingredienteRepository.findByIdIngrediente(id);
    }

    @Override
    @Transactional
    public void aggiornaIngrediente(Ingrediente ingredienteInstance) {
        ingredienteRepository.save(ingredienteInstance);

    }

    @Override
    @Transactional
    public void inserisciNuovaIngrediente(Ingrediente ingredienteInstance) {
        ingredienteRepository.save(ingredienteInstance);
    }

    @Override
    @Transactional
    public void rimuovi(Ingrediente ingredienteInstance) {
        ingredienteRepository.delete(ingredienteInstance);

    }

    @Override
    public List<Ingrediente> trovaPerNome(String nome) {

        return (List<Ingrediente>) ingredienteRepository.findIngredienteByNomeContains(nome);
    }
}
