package com.gestionetornei.resource;


import com.gestionetornei.model.Girone;
import com.gestionetornei.services.GironeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

@RestController
@RequestMapping("gironi")
public class GironeResources {

    @Autowired
    GironeService gironeService;


    @GetMapping(value = "/{id}", produces= MediaType.APPLICATION_JSON)
    public Girone getGirone(@PathVariable long id){
        System.out.println(id);
        return gironeService.caricaSingoloGirone(id);
    }

    @GetMapping(path = "/lista", produces=MediaType.APPLICATION_JSON)
    public List<Girone> listAll() {

        return gironeService.listAllGironi();
    }


    @PostMapping("/inserisci")
    @Produces(MediaType.APPLICATION_JSON)
    public boolean inserisciGirone(@RequestBody Girone gironeInput) {
        if (gironeInput.validate()) {
            System.out.println("sono qui nell'if");
            gironeService.inserisciNuovoGirone(gironeInput);
            return true;
        }
        System.out.println("sono fuori dall'if");
        return false;

    }

    @PutMapping(value = "/aggiorna")
    @Produces(MediaType.APPLICATION_JSON)
    public boolean updateGirone(@RequestBody Girone gironeInput) {
        if (gironeInput.validate()) {
            System.out.println("sono qui nell'if");
            gironeService.aggiornaGirone(gironeInput);
            return true;
        }
        System.out.println("sono fuori dall'if");
        return false;

    }

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON)
    public boolean delete(){
        return false;

    }




}
