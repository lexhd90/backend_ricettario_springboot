package com.gestionetornei.repositories;

import com.gestionetornei.model.Ricetta;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.regex.Pattern;

public class LeggiTxtUtility {
    public static List<String> ingredintiTxt() throws  IOException{


        List<String> lista = new ArrayList<>();
        Scanner scan = new Scanner(new File("C:\\Users\\Alessandro\\Desktop\\Nuova cartella (5)\\ing.txt"));
        scan.useDelimiter(Pattern.compile("_ING|\r\n"));
        while (scan.hasNext()) {
            String logicalLine = scan.next();
            if (logicalLine.matches("[a-zA-Z][a-zA-Z ]*")) {
                lista.add(logicalLine);
            }
        }
        scan.close();
        String sis = "ciao S1";
        if (sis.matches("[a-zA-Z][a-zA-Z ]*")) {
            System.out.println("APPOSTO");
        }


        String temp;
        List<String> lista2 = new ArrayList<String>();
        for (int i = 1; i < lista.size(); i++) {
            temp = lista.get(i);
            lista2.add(temp);
            i++;
        }


        List<String> newList = new ArrayList<String>();
        for (String element : lista2) {

            // If this element is not present in newList
            // then add it
            if (!newList.contains(element)) {
                newList.add(element);
                System.out.println(element);
            }
        }

        return newList;
    }

public static List<Ricetta> popolaRicette() throws IOException {




    List<String> listaNomi = new ArrayList<String>();
    List<String> listaPreparazioni = new ArrayList<>();
    List<Integer> perPersona = new ArrayList<>();
    List<String> ingredientiPresi = new ArrayList<>();
    List<String> tipoPiatti = new ArrayList<>();
    List<String> ingredientiUffucuali = new ArrayList<String>();
    List<String> scritturasuFile = new ArrayList<String>();
    int lineebianchePrep = 0;
    int lineebinacheIngr = 0;
    int lineebinacheTipo = 0;

    // PREPARAZIONE###########################################################################################

    String startToken = "-Preparazione_Completa"; //partenza
    String endToken = ":DBRicette"; //Arrivo
    boolean output = false;
    String preparazione = new String();
    Scanner scan = new Scanner(new File("C:\\Users\\Alessandro\\Desktop\\Nuova cartella (5)\\ricette.txt"));
    int index = 0;
    while (scan.hasNextLine()) {
        String line = scan.nextLine(); // mette nella stringa il contenuto della riga letta
        if (!output && line.indexOf(startToken) > -1) { //se il punto di arrivo corrisponde, setta output a true
            output = true;
            line = line.substring(line.indexOf(startToken) + startToken.length());
        } else if (output && line.indexOf(endToken) > -1) { //quando trova  il punto di arrivo, setta output a false
            output = false;
        }
        if (output) { //Quando è vero che ha trovato il match del punto partenza e arrivo

            if (!line.isEmpty()) {
                lineebianchePrep++;
                listaPreparazioni.add(line);
            } else {
                lineebianchePrep = 0;
            }
//            if (lineebianchePrep > 1)
////                System.out.println("prova :" + lineebianchePrep + line);
        }
        index++;
    }
    scan.close();

    // NOME###################################################################################################

    String startTokenNome = "-Nome";
    String endTokenNome = "-Tipo_Piatto";
    boolean outputNome = false;

    Scanner scans = new Scanner(new File("C:\\Users\\Alessandro\\Desktop\\Nuova cartella (5)\\ricette.txt"));
    while (scans.hasNextLine()) {
        String lineNome = scans.nextLine();
        if (!outputNome && lineNome.indexOf(startTokenNome) > -1) {
            outputNome = true;
            lineNome = lineNome.substring(lineNome.indexOf(startTokenNome) + startTokenNome.length());
        } else if (outputNome && lineNome.indexOf(endTokenNome) > -1) {
            outputNome = false;
        }

        if (outputNome) {
            if (!lineNome.isEmpty()) {
                listaNomi.add(lineNome);
            }
        }
    }
    scans.close();

    // NUMERO
    // PERSONE###################################################################################################

    String startTokenPersone = "-PersoneTotali";
    String endTokenPersone = "-NoteFinali";
    boolean outputPersone = false;

    Scanner scansPers = new Scanner(new File("C:\\Users\\Alessandro\\Desktop\\Nuova cartella (5)\\ricette.txt"));
    while (scansPers.hasNextLine()) {
        String linePers = scansPers.nextLine();
        if (!outputPersone && linePers.indexOf(startTokenPersone) > -1) {
            outputPersone = true;
            linePers = linePers.substring(linePers.indexOf(startTokenPersone) + startTokenPersone.length());
        } else if (outputPersone && linePers.indexOf(endTokenPersone) > -1) {
            outputPersone = false;
        }

        if (outputPersone) {
            if (!linePers.isEmpty()) {

                perPersona.add(Integer.valueOf(linePers));
            }
        }
    }
    scansPers.close();

    // INGREDIENTI###################################################################################################
    int index2 = 0;
    String startTokenIngr = "-Ing_Principale";
    String endTokenIngr = "-PersoneTotali";
    boolean outputIngr = false;

    Scanner scansIngr = new Scanner(new File("C:\\Users\\Alessandro\\Desktop\\Nuova cartella (5)\\ricette.txt"));
    while (scansIngr.hasNextLine()) {
        String lineIngr = scansIngr.nextLine();
        if (!outputIngr && lineIngr.indexOf(startTokenIngr) > -1) {
            outputIngr = true;
            lineIngr = lineIngr.substring(lineIngr.indexOf(startTokenIngr) + startTokenIngr.length());
        } else if (outputIngr && lineIngr.indexOf(endTokenIngr) > -1) {
            outputIngr = false;
        }

        if (outputIngr) {
            if (!lineIngr.isEmpty()) {
                ingredientiPresi.add(lineIngr);
            }
        }
        index2++;
    }
    scansIngr.close();

    // Tipo
    // PIATTO###################################################################################################
    int index3 = 0;
    String startTokenTipo = "-Tipo_Piatto";
    String endTokenTipo = "-Ing_Principale";
    boolean outputTipo = false;

    Scanner scansTipo = new Scanner(new File("C:\\Users\\Alessandro\\Desktop\\Nuova cartella (5)\\ricette.txt"));
    while (scansTipo.hasNextLine()) {
        String lineTipo = scansTipo.nextLine();
        if (!outputTipo && lineTipo.indexOf(startTokenTipo) > -1) {
            outputTipo = true;
            lineTipo = lineTipo.substring(lineTipo.indexOf(startTokenTipo) + startTokenTipo.length());
        } else if (outputTipo && lineTipo.indexOf(endTokenTipo) > -1) {
            outputTipo = false;
        }

        if (outputTipo) {
            if (!lineTipo.isEmpty()) {
                tipoPiatti.add(lineTipo);
            }
        }
        index3++;
    }
    scansTipo.close();

    // Ingredienti
    // UFFICIALI###################################################################################################
    int index4 = 0;
    String startTokenUff = "-Ingredienti";
    String endTokenUff = "-Preparazione_Completa";
    boolean outputUff = false;
    String ing = new String();
    String prova = new String();
    int lineebinacheUff = 0;
    int numero = 0;

    Scanner scansUff = new Scanner(new File("C:\\Users\\Alessandro\\Desktop\\Nuova cartella (5)\\ricette.txt"));
    while (scansUff.hasNextLine()) {

        String lineUff = scansUff.nextLine();
        if (!outputUff && lineUff.indexOf(startTokenUff) > -1) {
            outputUff = true;
            lineUff = lineUff.substring(lineUff.indexOf(startTokenUff) + startTokenUff.length());
//				System.out.println(lineebinacheUff);

        } else if (outputUff && lineUff.indexOf(endTokenUff) > -1) {
            outputUff = false;
        }

        if (outputUff) {
            lineebinacheUff++;
            if (!lineUff.isEmpty() && lineebinacheUff > 0)

                ing = ing + " " + lineUff;

            if (!ing.startsWith(lineUff)) {
                numero++;
                if (numero == 0) {
                    prova = ing;

                }
            }
        } else {
            if (!ing.isEmpty()) {
//                System.out.println(ing);
                ingredientiUffucuali.add(ing);
            }
            lineebinacheUff = 0;

            ing = "";
            numero = 0;
        }
        index4++;
    }
    scansUff.close();





        //AGGIUNTA RICETTA
    List<Ricetta> listaRicetta = new ArrayList<>();

    for(int i=0; i<listaNomi.size(); i++) {
        Ricetta ricettaTemp= new Ricetta();
        ricettaTemp.setNomericetta(listaNomi.get(i));
        ricettaTemp.setIngredientePrincipale(ingredientiPresi.get(i));
        ricettaTemp.setIngredientiGenerali(ingredientiUffucuali.get(i));
        ricettaTemp.setPerpersone(perPersona.get(i));
        ricettaTemp.setPreparazione(listaPreparazioni.get(i));
        ricettaTemp.setTipologiaPiatto(tipoPiatti.get(i));

        listaRicetta.add(ricettaTemp);
    }


        return listaRicetta;
    }

}
