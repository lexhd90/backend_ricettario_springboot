package com.gestionetornei.services;

import com.gestionetornei.model.Girone;
import com.gestionetornei.repositories.GironeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
public class GironeServiceImpl implements GironeService {

    @Autowired
    GironeRepository gironeRepository;

    @Override
    @Transactional(readOnly = true)
    public List<Girone> listAllGironi() {
        return (List<Girone>) gironeRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Girone caricaSingoloGirone(long id) {
        return gironeRepository.findById(id);
    }

    @Override
    @Transactional
    public void aggiornaGirone(Girone gironeInstance) {
        gironeRepository.save(gironeInstance);

    }

    @Override
    @Transactional
    public void inserisciNuovoGirone(Girone gironeInstance) {
        gironeRepository.save(gironeInstance);

    }

    @Override
    @Transactional
    public void rimuovi(Girone gironeInstance) {
        gironeRepository.delete(gironeInstance);

    }
}
