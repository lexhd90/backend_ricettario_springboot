package com.gestionetornei.services;

import com.gestionetornei.model.Evento;
import com.gestionetornei.repositories.EventoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EventoServiceImpl implements EventoService {
    @Autowired
    EventoRepository eventoRepository;

    @Override
    public List<Evento> listAllEventi() {
        return (List<Evento>) eventoRepository.findAll();
    }

    @Override
    public Evento caricaSingoloEvento(long id) {
        return  eventoRepository.findById(id);
    }

    @Override
    public void aggiornaEvento(Evento eventoInstance) {
        eventoRepository.save(eventoInstance);

    }

    @Override
    public void inserisciNuovoEvento(Evento eventoInstance) {
        eventoRepository.save(eventoInstance);
    }

    @Override
    public void rimuovi(Evento eventoInstance) {
        eventoRepository.delete(eventoInstance);
    }
}
