package com.gestionetornei.services;

import com.gestionetornei.model.Torneo;

import java.util.List;

public interface TorneoService  {

    public List<Torneo> listAllTornei();

    public Torneo caricaSingoloTorneo(long id);

    public void aggiorna(Torneo torneoInstance);

    public void inserisciNuovoTorneo(Torneo torneoInstance);

    public void rimuovi(Torneo torneoInstance);

    public List<Torneo> findByExample(Torneo torneoInstance);
}

