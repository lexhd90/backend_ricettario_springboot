package com.gestionetornei.resource;

import com.gestionetornei.model.Ingrediente;
import com.gestionetornei.repositories.viste.*;
import com.gestionetornei.services.IngredienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("ingredienti")
public class IngredienteResource {

    @Autowired
    IngredienteService ingredienteService;

    @Autowired
    VistaIngredienteServiceImpl vistaIngredienteService;


    @GetMapping(value = "/{id}", produces= MediaType.APPLICATION_JSON)
    public Ingrediente getIngrediente(@PathVariable Long id){
        System.out.println("------------------ SONO NEL DETTAGLIO INGREDIENTE!! ----------------------");
        return ingredienteService.caricaSingolaIngrediente(id);

    }



//    @GetMapping(path = "/lista", produces=MediaType.APPLICATION_JSON)
//    public List<Ingrediente> listAll() {
//
//        return ingredienteService.listAllIngredienti();
//    }

    @GetMapping(path = "/lista", produces=MediaType.APPLICATION_JSON)
    public List<VistaIngrediente> listAll() {
        System.out.println(".................SONO NELLA VISTA INGREDIENTI....................");
        return vistaIngredienteService.elencaVisteIngredienti();
    }


    @PostMapping("/inserisci")
    @Produces(MediaType.APPLICATION_JSON)
    public boolean inserisciIngrediente(@RequestBody Ingrediente ingredienteInput) {

        if (ingredienteInput!=null) {
            System.out.println("sono qui nell'if");
            ingredienteService.inserisciNuovaIngrediente(ingredienteInput);
            return true;
        }
        System.out.println("sono fuori dall'if");
        return false;

    }

    @PutMapping(value = "/aggiorna")
    @Produces(MediaType.APPLICATION_JSON)
    public boolean updateIngrediente(@RequestBody Ingrediente ingredienteInput) {
        if (ingredienteInput!=null) {
            System.out.println("sono qui nell'if");
            ingredienteService.aggiornaIngrediente(ingredienteInput);
            return true;
        }
        System.out.println("sono fuori dall'if");
        return false;

    }


    @PostMapping(path = "/cercaingredienti", produces=MediaType.APPLICATION_JSON)
    public List<VistaRicercaIngrediente> listaRicerca(@RequestBody String nomeIngrediente) {
        List<VistaRicercaIngrediente> ingredienti = new ArrayList<>();
        System.out.println(".................SONO NELLA RICERCA INGREDIENTI....................");
        ingredienti= vistaIngredienteService.elencaVisteRicercaIngredienti(nomeIngrediente);

        return ingredienti;
    }

       /* @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON)
        public boolean delete(){
            return false;

        }
*/
}
