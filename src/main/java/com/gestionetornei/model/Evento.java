package com.gestionetornei.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;

@Entity
@Data
public class Evento {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String nomeEvento;
    private LocalDate dataEvento;



    @ManyToOne
    @JoinColumn(name = "squadracasa_id")

    private Squadra squadraCasa;

    @ManyToOne
    @JoinColumn(name = "squadraospite_id")
    @JsonIgnoreProperties(value= {"tornei"})
    private Squadra squadraOspite;

    @ManyToOne
    @JsonIgnoreProperties(value = {"squadre","gironi", "eventi"})
    private Girone girone;


    @ManyToOne
    @JoinColumn(name = "vincitrice_id", nullable = true)
    @JsonIgnoreProperties(value= {"tornei", "eventiCasa", "eventiOspite"})
    private Squadra vincitrice;


    public boolean validate() {

        if ((nomeEvento.isEmpty() || nomeEvento == null) && (dataEvento==null)) {
            return false;
        }

        return true;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNomeEvento() {
        return nomeEvento;
    }

    public void setNomeEvento(String nomeEvento) {
        this.nomeEvento = nomeEvento;
    }

    public LocalDate getDataEvento() {
        return dataEvento;
    }

    public void setDataEvento(LocalDate dataEvento) {
        this.dataEvento = dataEvento;
    }

    public Squadra getSquadraCasa() {
        return squadraCasa;
    }

    public void setSquadraCasa(Squadra squadraCasa) {
        this.squadraCasa = squadraCasa;
    }

    public Squadra getSquadraOspite() {
        return squadraOspite;
    }

    public void setSquadraOspite(Squadra squadraOspite) {
        this.squadraOspite = squadraOspite;
    }

    public Girone getGirone() {
        return girone;
    }

    public void setGirone(Girone girone) {
        this.girone = girone;
    }

    public Squadra getVincitrice() {
        return vincitrice;
    }

    public void setVincitrice(Squadra vincitrice) {
        this.vincitrice = vincitrice;
    }
}
