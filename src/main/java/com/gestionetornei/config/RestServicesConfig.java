package com.gestionetornei.config;

import com.gestionetornei.resource.TorneoResource;
import javax.ws.rs.core.Application;

import java.util.HashSet;
import java.util.Set;

public class RestServicesConfig  extends Application {
    public Set<Class<?>> getClasses() {
        Set<Class<?>> classes = new HashSet<>();
        classes.add(TorneoResource.class);
        return classes;
    }
}
