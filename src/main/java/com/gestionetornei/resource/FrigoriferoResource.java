package com.gestionetornei.resource;

import com.gestionetornei.model.Frigorifero;
import com.gestionetornei.services.FrigoriferoService;
import com.gestionetornei.services.IngredienteService;
import com.gestionetornei.services.UtenteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

@RestController
@RequestMapping("frigoriferi")
public class FrigoriferoResource {

    @Autowired
    UtenteService utenteService;

    @Autowired
    FrigoriferoService frigoriferoService;

    @Autowired
    IngredienteService ingredienteService;


    @GetMapping(value = "/{id}", produces= MediaType.APPLICATION_JSON)
    public Frigorifero getFrigorifero(@PathVariable Long id){
        System.out.println("------------------ SONO NEL DETTAGLIO DEL FRIGO!! ----------------------");
        return frigoriferoService.caricaSingoloFrigorifero(id);
    }



    @GetMapping(path = "/lista", produces=MediaType.APPLICATION_JSON)
    public List<Frigorifero> listAll() {

        return  frigoriferoService.listAllFrigoriferi();
    }


    @PostMapping("/inserisci")
    @Produces(MediaType.APPLICATION_JSON)
    public boolean inserisciFrigorifero(@RequestBody Frigorifero frigoriferoInput) {
        System.out.println(frigoriferoInput);
        if (frigoriferoInput!=null) {
            System.out.println(frigoriferoInput.getIngredienti());
            System.out.println("sono qui nell'if");
            frigoriferoService.inserisciNuovoFrigorifero(frigoriferoInput);
            return true;
        }
        System.out.println("sono fuori dall'if");
        return false;

    }

    @PutMapping(value = "/aggiorna")
    @Produces(MediaType.APPLICATION_JSON)
    public boolean updateFrigorifero(@RequestBody Frigorifero frigoriferoInput) {
        if (frigoriferoInput!=null) {
            System.out.println("sono qui nell'if");
            frigoriferoService.aggiornaFrigorifero(frigoriferoInput);
            return true;
        }
        System.out.println("sono fuori dall'if");
        return false;

    }

       /* @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON)
        public boolean delete(){
            return false;

        }
*/
}
