package com.gestionetornei.repositories.viste;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import java.util.List;

public interface VistaRicercaIngredienteRepository extends CrudRepository<VistaRicercaIngrediente, Long>, QueryByExampleExecutor<VistaRicercaIngrediente> {


    public List<VistaRicercaIngrediente> findVistaRicercaIngredienteByNomeContains(String nome);
}
