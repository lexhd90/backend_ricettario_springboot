package com.gestionetornei.repositories.viste;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

public interface VistaRicettaRepository extends CrudRepository<VistaRicetta, Long>, QueryByExampleExecutor<VistaRicetta> {


}
