package com.gestionetornei.repositories;

import com.gestionetornei.model.Utente;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;


public interface UtenteRepository extends CrudRepository<Utente, Long>, QueryByExampleExecutor<Utente> {

    public Utente findByUsernameAndPassword(String username,String password);
    public Utente findUtenteById(Long id);
}