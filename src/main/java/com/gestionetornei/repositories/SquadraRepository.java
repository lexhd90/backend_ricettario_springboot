package com.gestionetornei.repositories;

import com.gestionetornei.model.Squadra;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import java.util.List;

public interface SquadraRepository extends CrudRepository<Squadra, Long>, QueryByExampleExecutor<Squadra> {

    Squadra findById(Integer id);

//    @Query("SELECT g FROM Girone g JOIN g.gironi s WHERE s.id=?1")
//    List<Squadra> findSquadraGirone
}
