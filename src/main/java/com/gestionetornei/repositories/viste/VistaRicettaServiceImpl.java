package com.gestionetornei.repositories.viste;

import com.gestionetornei.model.Ricetta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class VistaRicettaServiceImpl {
    @Autowired
    VistaRicettaRepository vistaRicettaRepository;

    @Transactional(readOnly = true)
    public List<VistaRicetta>elencaVisteRicette() {

        return (List<VistaRicetta>) vistaRicettaRepository.findAll();
    }

}
