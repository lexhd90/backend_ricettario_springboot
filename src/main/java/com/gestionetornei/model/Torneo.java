package com.gestionetornei.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
public class Torneo {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String nomeTorneo;

    private int numeroSquadre;

    @ManyToMany(mappedBy = "tornei")
    @JsonIgnoreProperties(value= {"tornei", "eventiCasa", "eventiOspite"})
    private List<Squadra> squadre = new ArrayList<>();

    @OneToMany(mappedBy ="torneo")
    @JsonIgnoreProperties(value= {"torneo", "eventi"})
    private List<Girone> gironi= new ArrayList<>();

    public Torneo () {}

    public Torneo(String nomeTorneo, int numeroSquadre, List<Squadra> squadre, List<Girone> gironi) {
        this.nomeTorneo = nomeTorneo;
        this.numeroSquadre = numeroSquadre;
        this.squadre = squadre;
        this.gironi = gironi;
    }


    public boolean validate() {

        if ((nomeTorneo.isEmpty() || nomeTorneo == null) || (numeroSquadre<0 || (numeroSquadre%2)>0)) {
            return false;
        }

        return true;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNomeTorneo() {
        return nomeTorneo;
    }

    public void setNomeTorneo(String nomeTorneo) {
        this.nomeTorneo = nomeTorneo;
    }

    public int getNumeroSquadre() {
        return numeroSquadre;
    }

    public void setNumeroSquadre(int numeroSquadre) {
        this.numeroSquadre = numeroSquadre;
    }

    public List<Squadra> getSquadre() {
        return squadre;
    }

    public void setSquadre(List<Squadra> squadre) {
        this.squadre = squadre;
    }

    public List<Girone> getGironi() {
        return gironi;
    }

    public void setGironi(List<Girone> gironi) {
        this.gironi = gironi;
    }

    @Override
    public String toString() {
        return "Torneo{" +
                "id=" + id +
                ", nomeTorneo='" + nomeTorneo + '\'' +
                ", numeroSquadre=" + numeroSquadre +
                ", squadre=" + squadre +
                ", gironi=" + gironi +
                '}';
    }
}
