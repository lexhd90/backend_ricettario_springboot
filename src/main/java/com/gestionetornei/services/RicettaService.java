package com.gestionetornei.services;

import com.gestionetornei.model.Ingrediente;
import com.gestionetornei.model.Ricetta;

import java.util.List;

public interface RicettaService {

    public List<Ricetta> listAllRicette();

    public Ricetta caricaSingolaRicetta(Long id);

    public void aggiornaRicetta(Ricetta ricettaInstance);

    public void inserisciNuovaRicetta(Ricetta ricettaInstance);

    public void rimuovi(Ricetta ricettaInstance);


    public List<Ricetta> ricercaNome(String input);
    public List<Ricetta> ricercaPerPersone(Integer input);
    public List<Ricetta> ricercaPerIngredientePrincipale(String input);
    public List<Ricetta> ricercaPerIngredientiGenerali(String input);
    public List<Ricetta> ricercaPerTipoPiatto(String input);
    public List<Ricetta> ricercaPerPreparazione(String input);

    public List<Ricetta> ricercaPerNomeETipo(String nome, String tipo);
    public List<Ricetta> ricercaPerNomeEIngredientePrincipale(String nome, String ingredientePrincipale);
    public List<Ricetta> ricercaPerTipoEIngredientePrincipale(String tipo, String ingredientePrincipale);
    public List<Ricetta> ricercaPerNomeTipoEIngredientePrincipale(String nome,  String ingredientePrincipale, String tipo);

    public List<Ricetta> ricercaPerIngredienti(List<Ingrediente> ingredienti);




}
