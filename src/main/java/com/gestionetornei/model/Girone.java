package com.gestionetornei.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
public class Girone {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String nomeGirone;

    @ManyToOne
    @JsonIgnoreProperties(value = {"squadre","gironi"})
    private Torneo torneo;

    @OneToMany(mappedBy="girone")
    @JsonIgnoreProperties(value = {"squadraCasa","squadraOspite","vincitrice", "girone"})
    private List<Evento> eventi=new ArrayList<>();


    public boolean validate() {

        if ((nomeGirone.isEmpty() || nomeGirone == null)) {
            return false;
        }

        return true;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNomeGirone() {
        return nomeGirone;
    }

    public void setNomeGirone(String nomeGirone) {
        this.nomeGirone = nomeGirone;
    }

    public Torneo getTorneo() {
        return torneo;
    }

    public void setTorneo(Torneo torneo) {
        this.torneo = torneo;
    }

    public List<Evento> getEventi() {
        return eventi;
    }

    public void setEventi(List<Evento> eventi) {
        this.eventi = eventi;
    }
}
