package com.gestionetornei.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
public class Ricetta {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long idricetta;

    private String nomericetta;
    private int perpersone;
    private String preparazione;
    private String linkImmagine;
    private String ingredientePrincipale;
    private String tipologiaPiatto;
    private String ingredientiGenerali;
    @ManyToMany(mappedBy = "ricette", cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JsonIgnoreProperties(value= {"ricette"})
    private List<Ingrediente> ingredienti;

    public Ricetta(){}

    public Long getIdricetta() {
        return idricetta;
    }

    public void setIdricetta(Long idricetta) {
        this.idricetta = idricetta;
    }

    public String getNomericetta() {
        return nomericetta;
    }

    public void setNomericetta(String nomericetta) {
        this.nomericetta = nomericetta;
    }

    public int getPerpersone() {
        return perpersone;
    }

    public void setPerpersone(int perpersone) {
        this.perpersone = perpersone;
    }

    public String getPreparazione() {
        return preparazione;
    }

    public void setPreparazione(String preparazione) {
        this.preparazione = preparazione;
    }

    public List<Ingrediente> getIngredienti() {
        return ingredienti;
    }

    public void setIngredienti(List<Ingrediente> ingredienti) {
        this.ingredienti = ingredienti;
    }

    public String getLinkImmagine() {
        return linkImmagine;
    }

    public void setLinkImmagine(String linkImmagine) {
        this.linkImmagine = linkImmagine;
    }

    public String getIngredientePrincipale() {
        return ingredientePrincipale;
    }

    public void setIngredientePrincipale(String ingredientePrincipale) {
        this.ingredientePrincipale = ingredientePrincipale;
    }

    public String getTipologiaPiatto() {
        return tipologiaPiatto;
    }

    public void setTipologiaPiatto(String tipologiaPiatto) {
        this.tipologiaPiatto = tipologiaPiatto;
    }

    public String getIngredientiGenerali() {
        return ingredientiGenerali;
    }

    public void setIngredientiGenerali(String ingredientiGenerali) {
        this.ingredientiGenerali = ingredientiGenerali;
    }
}
