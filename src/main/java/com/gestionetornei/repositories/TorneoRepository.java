package com.gestionetornei.repositories;

import com.gestionetornei.model.Torneo;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

public interface TorneoRepository  extends CrudRepository<Torneo, Long>, QueryByExampleExecutor<Torneo> {

   @Query("SELECT t FROM Torneo t left outer join t.squadre s where t.id=?1")
    Torneo findOneEager(long id);

   Torneo findById(long id);



}
