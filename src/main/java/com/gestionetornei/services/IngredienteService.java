package com.gestionetornei.services;

import com.gestionetornei.model.Ingrediente;

import java.util.List;

public interface IngredienteService {

    public List<Ingrediente> listAllIngredienti();

    public Ingrediente caricaSingolaIngrediente(Long id);

    public void aggiornaIngrediente(Ingrediente ingredienteInstance);

    public void inserisciNuovaIngrediente(Ingrediente ingredienteInstance);

    public void rimuovi(Ingrediente ingredienteInstance);

    public List<Ingrediente> trovaPerNome(String nome);
}
