package com.gestionetornei.repositories.viste;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

public interface VistaIngredienteRepository extends CrudRepository<VistaIngrediente, Long>, QueryByExampleExecutor<VistaIngrediente> {

}
