package com.gestionetornei.resource;

import com.gestionetornei.model.Torneo;
import com.gestionetornei.services.TorneoService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.util.List;

@Path("/tornei")
@Consumes({ "application/json" })
@Produces({ "application/json" })
public class ProvaRest {

    @Autowired
    TorneoService torneoService;

    @GET
    @Path("/lista")
    public List<Torneo> getTornei() {
    return torneoService.listAllTornei();
    }


}