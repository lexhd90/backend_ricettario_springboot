package com.gestionetornei.repositories;

import com.gestionetornei.model.Ingrediente;
import com.gestionetornei.model.Ricetta;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import java.util.List;

public interface RicettaRespository  extends CrudRepository<Ricetta, Long>, QueryByExampleExecutor<Ricetta> {

    public Ricetta findByIdricetta(Long id);

    public List<Ricetta> findRicettaByNomericettaContains(String input);
    public List<Ricetta> findRicettaByIngredientePrincipaleContains(String input);
    public List<Ricetta> findRicettaByPerpersone(Integer input);
    public List<Ricetta> findRicettaByTipologiaPiattoContains(String input);
    public List<Ricetta> findRicettaByIngredientiGeneraliContains(String input);
    public List<Ricetta> findRicettaByPreparazioneContains(String input);

    public List<Ricetta> findRicettaByNomericettaContainsAndTipologiaPiattoContains(String nome, String tipo);
    public List<Ricetta> findRicettaByNomericettaContainsAndIngredientePrincipaleContains(String nome, String ingredientePrincipale);
    public List<Ricetta> findRicettaByTipologiaPiattoContainsAndIngredientePrincipaleContains(String tipo, String ingredientePrincipale);
    public List<Ricetta> findRicettaByNomericettaContainsAndIngredientePrincipaleContainsAndTipologiaPiattoContains(String nome, String ingrediente, String tipo);
//    public List<Ricetta> findRicettaByIngredientiGeneraliContains(String ingredienti);

    public List<Ricetta> findRicettaByIngredienti(List<Ingrediente> ingredients);
}
