package com.gestionetornei.services;

import com.gestionetornei.model.Frigorifero;

import java.util.List;

public interface FrigoriferoService {

    public List<Frigorifero> listAllFrigoriferi();

    public Frigorifero caricaSingoloFrigorifero(Long id);

    public void aggiornaFrigorifero(Frigorifero frigoriferoInstance);

    public void inserisciNuovoFrigorifero(Frigorifero frigoriferoInstance);

    public void rimuovi(Frigorifero frigoriferoInstance);
}
