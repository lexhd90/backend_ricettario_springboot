package com.gestionetornei.services;

import com.gestionetornei.model.Squadra;
import com.gestionetornei.repositories.SquadraRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class SquadraServiceImpl implements SquadraService {

//    @PersistenceContext
//    private EntityManager entityManager;
    @Autowired
    private SquadraRepository squadraRepository;


    @Override
    @Transactional(readOnly = true)
    public List<Squadra> listAllSquadre() {
        return (List<Squadra>) squadraRepository.findAll();
    }


    @Transactional(readOnly = true)
    public Squadra caricaSingolaSquadra(Integer id) {
        return squadraRepository.findById(id);
    }

    @Override
    @Transactional
    public void aggiornaSquadra(Squadra squadraInstance) {
        squadraRepository.save(squadraInstance);

    }

    @Override
    @Transactional
    public void inserisciNuovaSquadra(Squadra squadraInstance) {
        squadraRepository.save(squadraInstance);

    }

    @Override
    @Transactional
    public void rimuovi(Squadra squadraInstance) {
        squadraRepository.delete(squadraInstance);

    }
}
