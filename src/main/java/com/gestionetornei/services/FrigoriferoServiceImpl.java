package com.gestionetornei.services;

import com.gestionetornei.model.Frigorifero;
import com.gestionetornei.repositories.FrigoriferoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
@Service
public class FrigoriferoServiceImpl implements FrigoriferoService {
    @Autowired
    FrigoriferoRepository frigoriferoRepository;

    @Override
    @Transactional
    public List<Frigorifero> listAllFrigoriferi() {
        return (List<Frigorifero>) frigoriferoRepository.findAll();
    }

    @Override
    @Transactional
    public Frigorifero caricaSingoloFrigorifero(Long id) {
        return frigoriferoRepository.findByIdfrigorifero(id);
    }

    @Override
    @Transactional
    public void aggiornaFrigorifero(Frigorifero frigoriferoInstance) {
        System.out.println("...........................Questo è il frigorifero.............................." + frigoriferoInstance);
        frigoriferoRepository.save(frigoriferoInstance);
    }

    @Override
    @Transactional
    public void inserisciNuovoFrigorifero(Frigorifero frigoriferoInstance) {
        frigoriferoRepository.save(frigoriferoInstance);
    }

    @Override
    @Transactional
    public void rimuovi(Frigorifero frigoriferoInstance) {
        frigoriferoRepository.delete(frigoriferoInstance);
    }
}
