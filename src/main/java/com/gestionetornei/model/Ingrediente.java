package com.gestionetornei.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Ingrediente {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long idIngrediente;

    private String nome;

    private String linkImmagine;

    @ManyToMany(cascade = {CascadeType.DETACH})
    @JsonIgnoreProperties(value = {"ingredienti"})
    private List<Ricetta> ricette;

    @ManyToMany
    @JsonIgnoreProperties(value = {"ingredienti", "Utente"})
    private List<Frigorifero> frigoriferi;

    public Ingrediente(){}

    public Long getIdIngrediente() {
        return idIngrediente;
    }

    public void setIdIngrediente(Long idIngrediente) {
        this.idIngrediente = idIngrediente;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getLinkImmagine() {
        return linkImmagine;
    }

    public void setLinkImmagine(String linkImmagine) {
        this.linkImmagine = linkImmagine;
    }

    public List<Ricetta> getRicette() {
        return ricette;
    }

    public void setRicette(List<Ricetta> ricette) {
        this.ricette = ricette;
    }

    public List<Frigorifero> getFrigoriferi() {
        return frigoriferi;
    }

    public void setFrigoriferi(List<Frigorifero> frigoriferi) {
        this.frigoriferi = frigoriferi;
    }
}
