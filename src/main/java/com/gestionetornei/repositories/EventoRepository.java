package com.gestionetornei.repositories;

import com.gestionetornei.model.Evento;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

public interface EventoRepository extends CrudRepository<Evento, Long>, QueryByExampleExecutor<Evento> {

    Evento findById(long id);
}
