package com.gestionetornei.services;

import com.gestionetornei.model.Squadra;

import java.util.List;

public interface SquadraService {

    public List<Squadra> listAllSquadre();

    public Squadra caricaSingolaSquadra(Integer id);

    public void aggiornaSquadra(Squadra squadraInstance);

    public void inserisciNuovaSquadra(Squadra squadraInstance);

    public void rimuovi(Squadra squadraInstance);
}
