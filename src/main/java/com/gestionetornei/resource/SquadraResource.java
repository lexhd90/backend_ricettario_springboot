package com.gestionetornei.resource;

import com.gestionetornei.model.Squadra;
import com.gestionetornei.services.SquadraService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;
@RestController
@RequestMapping("squadre")
public class SquadraResource {



        @Autowired
        SquadraService squadraService;


        @GetMapping(value = "/{id}", produces= MediaType.APPLICATION_JSON)
        public Squadra getSquadra(@PathVariable Integer id){
            System.out.println(id);
            return squadraService.caricaSingolaSquadra(id);

        }



        @GetMapping(path = "/lista", produces=MediaType.APPLICATION_JSON)
        public List<Squadra> listAll() {

            return squadraService.listAllSquadre();
        }


        @PostMapping("/inserisci")
        @Produces(MediaType.APPLICATION_JSON)
        public boolean inserisciSquadra(@RequestBody Squadra squadraInput) {
            if (squadraInput.validate()) {
                System.out.println("sono qui nell'if");
                squadraService.inserisciNuovaSquadra(squadraInput);
                return true;
            }
            System.out.println("sono fuori dall'if");
            return false;

        }

        @PutMapping(value = "/aggiorna")
        @Produces(MediaType.APPLICATION_JSON)
        public boolean updateSquadra(@RequestBody Squadra squadraInput) {
            if (squadraInput.validate()) {
                System.out.println("sono qui nell'if");
               squadraService.aggiornaSquadra(squadraInput);
                return true;
            }
            System.out.println("sono fuori dall'if");
            return false;

        }

       /* @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON)
        public boolean delete(){
            return false;

        }
*/

    }
