package com.gestionetornei.repositories.viste;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "my_view")
public class VistaRicetta {

    @Id
    private Long idricetta;

    private String nomericetta;
    private int perpersone;
    private String preparazione;
    private String linkImmagine;
    private String ingredientePrincipale;
    private String tipologiaPiatto;
    private String ingredientiGenerali;

    public Long getIdricetta() {
        return idricetta;
    }

    public void setIdricetta(Long idricetta) {
        this.idricetta = idricetta;
    }

    public String getNomericetta() {
        return nomericetta;
    }

    public void setNomericetta(String nomericetta) {
        this.nomericetta = nomericetta;
    }

    public int getPerpersone() {
        return perpersone;
    }

    public void setPerpersone(int perpersone) {
        this.perpersone = perpersone;
    }

    public String getPreparazione() {
        return preparazione;
    }

    public void setPreparazione(String preparazione) {
        this.preparazione = preparazione;
    }

    public String getLinkImmagine() {
        return linkImmagine;
    }

    public void setLinkImmagine(String linkImmagine) {
        this.linkImmagine = linkImmagine;
    }

    public String getIngredientePrincipale() {
        return ingredientePrincipale;
    }

    public void setIngredientePrincipale(String ingredientePrincipale) {
        this.ingredientePrincipale = ingredientePrincipale;
    }

    public String getTipologiaPiatto() {
        return tipologiaPiatto;
    }

    public void setTipologiaPiatto(String tipologiaPiatto) {
        this.tipologiaPiatto = tipologiaPiatto;
    }

    public String getIngredientiGenerali() {
        return ingredientiGenerali;
    }

    public void setIngredientiGenerali(String ingredientiGenerali) {
        this.ingredientiGenerali = ingredientiGenerali;
    }
}
