package com.gestionetornei.resource;


import com.gestionetornei.model.Ingrediente;
import com.gestionetornei.model.Ricetta;
import com.gestionetornei.model.Utente;
import com.gestionetornei.repositories.LeggiTxtUtility;
import com.gestionetornei.services.IngredienteService;
import com.gestionetornei.services.RicettaService;
import com.gestionetornei.services.UtenteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("login")
public class LoginResource {

    @Autowired
    UtenteService utenteService;

    @Autowired
    RicettaService ricettaService;
    @Autowired
    IngredienteService ingredienteService;

    @PostMapping(value = "/accesso")
    @Produces(MediaType.APPLICATION_JSON)
    public Utente login(@RequestBody Utente utente) {
        if(utente==null){
            System.out.println("ERRORE NELL'ACCESSO, UTENTE NULL");
            return utente;
        }
        System.out.println("..........................SONO NEL METODO DI LOGIN!!!! " + utente);
        Utente utenteDaLogin = utenteService.eseguiAccesso(utente.getUsername(), utente.getPassword());
        if (utenteDaLogin != null) {
            System.out.println("Accesso eseguito :" + utenteDaLogin);
            return utenteDaLogin;
//            return Response.ok().entity(utenteDaLogin).build();
        }
        System.out.println("Questo è l'utente che non esiste: " + utenteDaLogin);
        System.out.println("Accesso NON eseguito");

//        return Response.serverError().build();
        return utenteDaLogin;
    }


    @PostMapping("/registrazione")
    @Produces(MediaType.APPLICATION_JSON)
    public Response inserisciUtente(@RequestBody Utente utenteInput) {
        System.out.println(utenteInput);
//        if ((torneoInput.validate()) && (torneoInput.getSquadre().size()== torneoInput.getNumeroSquadre())) {

        Utente daLog = utenteService.eseguiAccesso(utenteInput.getUsername(), utenteInput.getPassword());
        if (daLog==null || !daLog.getUsername().equals(utenteInput.getUsername())) {
            System.out.println("sono nell'inserimento");
            utenteService.inserisciNuovo(utenteInput);
            return Response.ok("inserimento effettuato").build();
        }
        System.out.println(daLog);
        System.out.println("sono nell'errore");
        return Response.serverError().build();
    }

    @GetMapping(value = "/{id}", produces= MediaType.APPLICATION_JSON)
    public Utente getUtente(@PathVariable Long id){
        System.out.println("------------------ SONO NEL DETTAGLIO!! ----------------------");
        Utente utenteSingolo = utenteService.caricaSingoloUtente(id);
        System.out.println(utenteSingolo);
        return utenteSingolo;

    }

//    @GetMapping(value = "/popolaingredienti", produces= MediaType.APPLICATION_JSON)
//    public boolean popola() throws IOException {
//        List<String> ingredienti= LeggiTxtUtility.ingredintiTxt();
//
//        for(String nomeIngrediente: ingredienti){
//            Ingrediente ing = new Ingrediente();
//            ing.setNome(nomeIngrediente);
//            ingredienteService.inserisciNuovaIngrediente(ing);
//
//        }
//        return true;
//
//    }


//        @GetMapping(value = "/popolaricette", produces= MediaType.APPLICATION_JSON)
//    public boolean popolaRicettes() throws IOException {
//        List<Ricetta> ricette= LeggiTxtUtility.popolaRicette();
//
//            for (Ricetta ricItem:
//                    ricette ) {
//                ricettaService.inserisciNuovaRicetta(ricItem);
////                System.out.println
////                        ("Nome: " +ricItem.getNomericetta()+ " \n" +
////                                "Per Persone: " +ricItem.getPerpersone()+ " \n" +
////                                "Tipo: " +ricItem.getTipologiaPiatto()+ " \n" +
////                                "Ing Principale: " +ricItem.getIngredientePrincipale()+ " \n" +
////                                "ingredienti: " +ricItem.getIngredientiGenerali()+ " \n" +
////                                "Preparazione: " +ricItem.getPreparazione()+ " \n" );
////
////                System.out.println("\n" +"\n" + "\n" +"\n" +"\n" + "\n" );
//
//            }
//
////        for(String nomeIngrediente: ingredienti){
////            Ingrediente ing = new Ingrediente();
////            ing.setNome(nomeIngrediente);
////            ingredienteService.inserisciNuovaIngrediente(ing);
////
////        }
//        return true;
//
//    }


//    @GetMapping(path = "/popolaingredienti", produces=MediaType.APPLICATION_JSON)
//    public boolean listAll() {
//
//        List<Ricetta> ricette = new ArrayList<>();
//        List<Ingrediente> ingredienti = (List<Ingrediente>) ingredienteService.listAllIngredienti();
//
//
//
//        for(Ingrediente ingItem: ingredienti){
//            ricette = (ricettaService.ricercaPerIngredientiGenerali(ingItem.getNome()));
//            ingItem.setRicette(ricette);
//            ingredienteService.aggiornaIngrediente(ingItem);
//        }
//        return true;
//    }



}
