package com.gestionetornei.resource;

import com.gestionetornei.model.Evento;
import com.gestionetornei.services.EventoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

@RestController
@RequestMapping("eventi")
public class EventoResource {



    @Autowired
    EventoService eventoService;


    @GetMapping(value = "/{id}", produces= MediaType.APPLICATION_JSON)
    public Evento getEvento(@PathVariable long id){
        System.out.println(id);
        return eventoService.caricaSingoloEvento(id);

    }


    @GetMapping(path = "/lista", produces=MediaType.APPLICATION_JSON)
    public List<Evento> listAll() {
        return eventoService.listAllEventi();
    }


    @PostMapping("/inserisci")
    @Produces(MediaType.APPLICATION_JSON)
    public boolean inserisciEvento(@RequestBody Evento eventoInput) {
        if (eventoInput.validate()) {
            System.out.println("sono qui nell'if");
            eventoService.inserisciNuovoEvento(eventoInput);
            return true;
        }
        System.out.println("sono fuori dall'if");
        return false;

    }

    @PutMapping(value = "/aggiorna")
    @Produces(MediaType.APPLICATION_JSON)
    public boolean updateEvento(@RequestBody Evento eventoInput) {
        System.out.println("id dell'evento: " +eventoInput.getId());
        if (eventoInput.validate()) {
            System.out.println("sono qui nell'if");
            eventoService.aggiornaEvento(eventoInput);
            return true;
        }
        System.out.println("sono fuori dall'if");
        return false;

    }

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON)
    public boolean delete(){
        return false;


    }

}
