package com.gestionetornei.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;
import javax.persistence.Id;
import java.util.ArrayList;

import java.util.List;


@Entity
@Data
public class Squadra {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;



    private String nome;

    @ManyToMany
    @JsonIgnoreProperties(value = {"squadre","gironi"})
    private List<Torneo> tornei = new ArrayList<>();
/*
    @OneToMany(mappedBy = "squadraCasa")
    @JsonIgnoreProperties(value= {"tornei", "eventiCasa", "eventiOspite"})
    private List<Evento> eventiCasa=new ArrayList<>();

    @OneToMany(mappedBy = "squadraOspite")
    private List<Evento> eventiOspite = new ArrayList<>();*/



    public Squadra () {}

    public Squadra(String nome, List<Torneo> tornei, List<Evento> eventiCasa, List<Evento> eventiOspite) {
        this.nome = nome;
        this.tornei = tornei;
/*        this.eventiCasa = eventiCasa;
        this.eventiOspite = eventiOspite;*/
    }

    public boolean validate() {

        if ((nome.isEmpty() || nome == null)) {
            return false;
        }

        return true;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<Torneo> getTornei() {
        return tornei;
    }

    public void setTornei(List<Torneo> tornei) {
        this.tornei = tornei;
    }
}
