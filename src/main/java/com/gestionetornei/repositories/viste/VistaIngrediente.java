package com.gestionetornei.repositories.viste;

import javax.persistence.*;

@Entity
@Table(name = "my_view_ingrediente")
public class VistaIngrediente {


    @Id
    private Long idIngrediente;

    private String nome;

    private String linkImmagine;

    public Long getIdIngrediente() {
        return idIngrediente;
    }

    public void setIdIngrediente(Long idIngrediente) {
        this.idIngrediente = idIngrediente;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getLinkImmagine() {
        return linkImmagine;
    }

    public void setLinkImmagine(String linkImmagine) {
        this.linkImmagine = linkImmagine;
    }
}
