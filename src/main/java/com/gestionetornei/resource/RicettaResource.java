package com.gestionetornei.resource;

import com.gestionetornei.model.Ingrediente;
import com.gestionetornei.model.Ricetta;
import com.gestionetornei.repositories.viste.VistaRicetta;
import com.gestionetornei.repositories.viste.VistaRicettaServiceImpl;
import com.gestionetornei.services.IngredienteService;
import com.gestionetornei.services.RicettaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("ricette")
public class RicettaResource {



    @Autowired
    RicettaService ricettaService;

    @Autowired
    VistaRicettaServiceImpl vistaRicettaService;

    @Autowired
    IngredienteService ingredienteService;


    @GetMapping(value = "/{id}", produces= MediaType.APPLICATION_JSON)
    public Ricetta getRicetta(@PathVariable Long id){
        System.out.println("------------------ SONO NEL DETTAGLIO RICETTA!! ----------------------");
        return ricettaService.caricaSingolaRicetta(id);

    }


//    @GetMapping(path = "/lista", produces=MediaType.APPLICATION_JSON)
//    public List<Ricetta> listAll() {
//
//        return ricettaService.listAllRicette();
//    }

        @GetMapping(path = "/lista", produces=MediaType.APPLICATION_JSON)
    public List<VistaRicetta> listAll() {

        return vistaRicettaService.elencaVisteRicette();
    }


    @PostMapping("/inserisci")
    @Produces(MediaType.APPLICATION_JSON)
    public boolean inserisciRicetta(@RequestBody Ricetta ricettaInput) {
        System.out.println(ricettaInput);
        if (ricettaInput!=null) {
           System.out.println(ricettaInput.getIngredienti());
            System.out.println("sono qui nell'if");
            ricettaService.inserisciNuovaRicetta(ricettaInput);
            return true;
        }
        System.out.println("sono fuori dall'if");
        return false;

    }

    @PutMapping(value = "/aggiorna")
    @Produces(MediaType.APPLICATION_JSON)
    public boolean updateRicetta(@RequestBody Ricetta ricettaInput) {
        if (ricettaInput!=null) {
            System.out.println("sono qui nell'if");
            ricettaService.aggiornaRicetta(ricettaInput);
            return true;
        }
        System.out.println("sono fuori dall'if");
        return false;

    }

       /* @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON)
        public boolean delete(){
            return false;

        }
*/


    @PostMapping(value = "/cercaRicetta", produces= MediaType.APPLICATION_JSON)
    public List<Ricetta> getNomeRicetta(@RequestBody Ricetta input){
        System.out.println("------------------ SONO NELLA RICERCA!! ----------------------");

        System.out.println(input);
        List<Ricetta> ricette = new ArrayList<>();
        if( (input.getNomericetta()!=null && !(input.getNomericetta().isEmpty()))
                && (input.getTipologiaPiatto()==null || input.getTipologiaPiatto().isEmpty())
                && (input.getIngredientePrincipale()==null || input.getIngredientePrincipale().isEmpty())) {
            System.out.println("SOLO Nome");
            return ricettaService.ricercaNome(input.getNomericetta());

//            return  Response.ok().entity(ricette).build();
        }
        if( (input.getIngredientePrincipale()!=null &&  !(input.getIngredientePrincipale().isEmpty()))
                && (input.getTipologiaPiatto()==null || input.getTipologiaPiatto().isEmpty())
                && (input.getNomericetta()==null || input.getNomericetta().isEmpty())) {

            System.out.println("SOLO INGRE");
            return ricettaService.ricercaPerIngredientePrincipale(input.getIngredientePrincipale());
//            return  Response.ok().entity(ricette).build();
        }
        if( (input.getTipologiaPiatto()!=null &&  !(input.getTipologiaPiatto().isEmpty()))
            && (input.getIngredientePrincipale()==null || input.getIngredientePrincipale().isEmpty())
            && (input.getNomericetta()==null ||  input.getNomericetta().isEmpty())   ) {

            System.out.println("SOLO TIPO");
            return ricettaService.ricercaPerTipoPiatto(input.getTipologiaPiatto());
//            return  Response.ok().entity(ricette).build();
        }

        if( (input.getTipologiaPiatto()!=null &&  !(input.getTipologiaPiatto().isEmpty())) && (input.getNomericetta()!=null && !(input.getNomericetta().isEmpty()))
               ) {

            System.out.println("SOLO TIPO E NOME");
            return ricettaService.ricercaPerNomeETipo(input.getNomericetta(), input.getTipologiaPiatto()) ;
//            return  Response.ok().entity(ricette).build();
        }

        if( (input.getTipologiaPiatto()!=null &&  !(input.getTipologiaPiatto().isEmpty())) && (input.getIngredientePrincipale()!=null &&  !(input.getIngredientePrincipale().isEmpty()))  ){

            System.out.println("SOLO TIPO E INGREDIENTE");
            return ricettaService.ricercaPerTipoEIngredientePrincipale(input.getTipologiaPiatto(), input.getIngredientePrincipale());
        }

        if( (input.getIngredientePrincipale()!=null &&  !(input.getIngredientePrincipale().isEmpty())) && (input.getNomericetta()!=null && !(input.getNomericetta().isEmpty())) ){
            System.out.println("SOLO NOME E INGREDIENTE");
            return ricettaService.ricercaPerNomeEIngredientePrincipale(input.getNomericetta(), input.getIngredientePrincipale());
        }

        return ricette;
    }


    @PostMapping(path = "/cercaingredienti", produces=MediaType.APPLICATION_JSON)
    public List<Ricetta> listaFrigo(@RequestBody List<Ingrediente> ingredienti) {
        List<Ricetta> ricette = new ArrayList<>();
        System.out.println(".................SONO NELLA RICERCA INGREDIENTI....................");
        if(ingredienti!= null)
         ricettaService.ricercaPerIngredienti(ingredienti);



        return ricette;
    }
}
